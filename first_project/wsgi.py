#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import os
import sys
PROJECT_PATH=os.path.abspath(os.path.dirname(__file__))
sys.path.append("/home/hosein/Documents/django_refactoring/codes/")
if PROJECT_PATH not in sys.path:
    sys.path.append(PROJECT_PATH)
 

import sirup.core.handlers.wsgi
application = sirup.core.handlers.wsgi.WSGIHandler()
if __name__ == "__main__":
    from wsgiref.simple_server import make_server
    httpd = make_server('', 8000, application)
    print "Serving on port 8000..."
    httpd.serve_forever()
