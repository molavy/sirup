# -*- coding: utf-8 -*-

class BaseHandler(object):
    pass

class WSGIHandler(BaseHandler):
    def __call__(self, environ, start_response):
        start_response('200 OK', [('Content-Type', 'text/plain')])
        yield 'Hello World\n'
